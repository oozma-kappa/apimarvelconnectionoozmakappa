# API Marvel Connection Oozma Kappa

El siguiente proyecto se refiere a la conexión hacia la API Marvel para la obtención de datos y procesamiento de estos en un Data Lake. Además de la interacción con la consola de AWS y algunos servicios que ofrece.

## API Marvel

La API de Marvel Comics es una herramienta para ayudar a los desarrolladores a crear sitios web y aplicaciones utilizando datos de los más de 70 años de la era de los cómics Marvel. Es un servicio RestFul que proporciona métodos de acceso a los recursos especificados por la URL canónica, los conjuntos de recursos se pueden filtrar según varios criterios y la codificación es en objetos JSON. La API ofrece los siguientes recursos:
* **Cómics:** ediciones individuales de cómics impresos y digitales, colecciones y novelas gráficas.
* **Serie de cómics:** grupos de cómics numerados secuencialmente 
* **Historias cómicas:** componentes indivisibles y reutilizables de las historietas.
* **Eventos cómicos y crossovers:** grandes historias que alteran el universo.
* **Creadores:** mujeres, hombres y organizaciones que crean cómics.
* **Personajes:** mujeres, hombres, organizaciones, especies alienígenas, deidades, animales, entidades no corporales, entre otras.

Siendo el endpoint base de la API el siguiente _http(s)://gateway.marvel.com/_, los endpoints a utilizar en las peticiones a la Api de Marvel, serán:

* **GET** _/v1/public/characters_: Lista de personajes
* **GET** _/v1/public/comics_: Lista de comics
* **GET** _/v1/public/creators_: Lista de creadores
* **GET** _/v1/public/events_: Lista de eventos
* **GET** _/v1/public/series_: Lista de series
* **GET** _/v1/public/stories_: Lista de historias

## Estrategias 

* **Lenguaje de programación:**  Python Versión 3.9.0
* **Librerías:**  Principalmente Hashlib, Requests,Json,Boto3.
* **Herramientas:** IDE PyCharm Versión 2020.2.3, servicios de Amazon (Lambda, CloudWatch, S3, EC2, y los que puedan ser necesarios durante el desarrollo), repositorio remoto GitLab, posible utilización de Postman para verificar peticiones a la API.
* **Ayuda:** Documentación de Amazon, documentación de API Marvel.

## Instalación Librerías

La librería Hashlib y Json no necesitan instalación, sin embargo, se debe hacer instalación de la librería Requests y Boto3 para utilizar servicios AWS

```bash
pip install requests
pip install boto3
```

Y el uso sería la importación de las librerías 

```python
import hashlib
import requests
import json 
import boto3 
```
