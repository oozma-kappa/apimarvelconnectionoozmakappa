# Librerias necesarias
import requests
import hashlib
import boto3
import json

# Claves de acceso y variables creadas para las peticiones a la API y servicios AWS
ACCESSKEYID = 'AKIAU7NI727M42UFHQ7H'
SECRETACCESSKEY = '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'
PUBLICKEYAPI = '7db552d10cbfde93a795dfd2aa18fa18'
PRIVATEKEYAPI = '0d99d47355cdca6b93be9579efc8e570524d244b'
TSAPI = '1'
BASEAPI = 'https://gateway.marvel.com/v1/public/'
NOMBREBUCKET = 'nabibucket'


def realizarpeticion(base, ts, nombreendpoint, publickey, privatekey, accesskeyid, secretacceskey, numeropeticiones,
                     nombrebucket):
    """
    La funcion permite iterar a travez del numero de peticiones
    Utiliza las claves y parametros para obtener las respuestas de API Marvel
    y los datos obtenidos se llevan a una funcion para guardarlos

    :param base: url base de la api marvel
    :param ts:
    :param nombreendpoint: punto final de la url para peticiones
    :param publickey: llave publica del usuario en la api marvel
    :param privatekey: llave private del usuario en la api marvel
    :param accesskeyid: llave de acceso del iam para servicios AWS
    :param secretacceskey: llave secreta de acceso del iam para servicios AWS
    :param numeropeticiones: cantidad de peticiones requeridas
    :param nombrebucket: bucket donde se cargar los archivos
    :return: no retorna
    """
    h = hashlib.md5((ts + privatekey + publickey).encode()).hexdigest()
    offset = 0
    limit = 100
    directorioarchivo = 'api/marvel/requests/' + nombreendpoint + '/datospeticion'
    directorioarchivobandera = directorioarchivo
    nombrearchivo = 'datospeticion.json'

    for i in range(numeropeticiones):
        # Peticion get de la api
        resultado = requests.get(base + nombreendpoint, params={'apikey': publickey, 'ts': ts, 'hash': h, 'limit': limit
                                 , 'offset': offset}).json()
        directorioarchivo = directorioarchivo + str(i) + '.json'
        guardarrespuestabucket(directorioarchivo, accesskeyid, secretacceskey, nombrebucket, nombrearchivo, resultado)

        # Aumento del offset para la paginacion
        offset = offset + limit
        directorioarchivo = directorioarchivobandera

    print('Peticion a ' + base + nombreendpoint + ' OK!')


def guardarrespuestabucket(nombrellave, accesskeyid, secretacceskey, nombrebucket, nombrearchivo, datos):
    """
    La funcion permite el acceso al servicio s3 de almacenamiento
    y guarda el archivo json resultante en el bucket seleccionado

    :param nombrellave: nombre a guardar dentro del bucket
    :param accesskeyid: llave de acceso del iam para el servicio s3
    :param secretacceskey: llave secreta de acceso del iam para el servicio s3
    :param nombrebucket: bucket donde se cargar los archivos
    :param nombrearchivo: archivo json creado
    :param datos: resultado de la peticion
    :return: no retorna
    """

    # Se inicia la sesion por media de las llaves de acceso de AWS
    session = boto3.session.Session(aws_access_key_id=accesskeyid,aws_secret_access_key=secretacceskey)

    # Se accede al servicio de almacenamiento
    s3 = session.resource('s3')

    with open(nombrearchivo, 'w') as file:
        json.dump(datos, file)

    data = open(nombrearchivo, 'rb')
    s3.Bucket(nombrebucket).put_object(Key=nombrellave, Body=data)

# End point recogidos de la documentacion de la API Marvel
listsendpoints = ['characters', 'comics', 'creators', 'events', 'series', 'stories']
listnumeropeticiones = [13, 20, 53, 1, 11, 40]

for i in range(0, len(listsendpoints)):
    realizarpeticion(BASEAPI, TSAPI, listsendpoints[i], PUBLICKEYAPI, PRIVATEKEYAPI, ACCESSKEYID, SECRETACCESSKEY,
                     listnumeropeticiones[i], NOMBREBUCKET)

print('OK!')
