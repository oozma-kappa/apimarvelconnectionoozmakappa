# Librerias necesarias

import boto3
import json
import psycopg2
import datetime
import time

# Claves de acceso y variables creadas para las peticiones a la API y servicios AWS
ACCESSKEYID = 'AKIAU7NI727M42UFHQ7H'
SECRETACCESSKEY = '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'
NOMBREBUCKET = 'nabibucket'
USERDB ="postgres"
PASSWORDDB = "andersonramirez"
HOSTDB = "proyectobi.cblj0topjbz6.us-east-2.rds.amazonaws.com"
PORTDB = "5432"
DATABASE = "proyectobi"
BASEAPI = 'https://gateway.marvel.com/v1/public/'
NOMBREAPI = 'MARVEL'
connectionbd = psycopg2.connect(user=USERDB, password=PASSWORDDB, host=HOSTDB, port=PORTDB, database=DATABASE)
cursorbd = connectionbd.cursor()


def funcioncargabd(accesskeyid, secretacceskey, nombrebucket, nombreendpoint, cantidaditeraciones, connection, cursor,
                   baseapi, nombreapi):
    """
    La funcion recorre los archivos del bucket por medio de la cantidad de iteraciones e inserta con querys
    dentro de la base de datos creada por el servicio RDS
    :param accesskeyid: llave de acceso del iam para servicios AWS
    :param secretacceskey: llave secreta de acceso del iam para servicios AWS
    :param nombrebucket: bucket donde se cargar los archivos
    :param nombreendpoint: punto final de la api marvel
    :param cantidaditeraciones: cantidad de archivos necesarios del bucket
    :param connection: conexion a la base de datos
    :param cursor: permite ejecutar consultas
    :param baseapi: url de la api marvel
    :param nombreapi: MARVEL
    :return: no retorna
    """
    # Se inicia la sesion por media de las llaves de acceso de AWS
    s3 = boto3.client('s3', aws_access_key_id=accesskeyid, aws_secret_access_key=secretacceskey)

    directorioarchivo = 'api/marvel/requests/' + nombreendpoint + '/datospeticion'
    directorioarchivobandera = directorioarchivo

    for i in range(cantidaditeraciones):
        directorioarchivo = directorioarchivo + str(i) + '.json'
        respuestabucket = s3.get_object(Bucket=nombrebucket, Key=directorioarchivo)
        tiempoconsulta = datetime.datetime(int(time.strftime("%Y")), int(time.strftime("%m")), int(time.strftime("%d")),
                                           int(time.strftime("%H")), int(time.strftime("%M")), int(time.strftime("%S")))
        directorioarchivo = directorioarchivobandera

        contenido = respuestabucket['Body']
        objetojson = json.loads(contenido.read())
        data = objetojson['data']
        results = data['results']

        for j in range(len(results)):
            id = results[j]['id']

            if nombreendpoint == 'comics':
                baseapi = baseapi + 'comics'
                querycomic = """INSERT INTO comic(idcomicapi,digitalid,title,issuenumber,variantdescription,description,
                modified,format,pagecount,resourceuri,upc,timeconsult,timesave,endpoint,nameapi) 
                VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
                tiempoguardadocomic = datetime.datetime(int(time.strftime("%Y")), int(time.strftime("%m")),
                                                        int(time.strftime("%d")), int(time.strftime("%H")),
                                                        int(time.strftime("%M")),int(time.strftime("%S")))
                recordinsertcomic = (results[j]['id'], results[j]['digitalId'], results[j]['title'],
                                     results[j]['issueNumber'], results[j]['variantDescription'],
                                     results[j]['description'], results[j]['modified'], results[j]['format'],
                                     results[j]['pageCount'], results[j]['resourceURI'], results[j]['upc'],
                                     str(time.mktime(tiempoconsulta.timetuple())),
                                     str(time.mktime(tiempoguardadocomic.timetuple())), baseapi + 'comics', nombreapi)
                cursor.execute(querycomic, recordinsertcomic)
                connection.commit()

                textobjects = results[j]['textObjects']
                for k in range(len(textobjects)):
                    queryobjectcomic = """INSERT INTO objectcomic(idcomic,typeobject,languageobject,textobject)
                    VALUES(%s,%s,%s,%s)"""
                    recordinsertobject = (id,textobjects[k]['type'],textobjects[k]['language'],
                                          textobjects[k]['text'])
                    cursor.execute(queryobjectcomic, recordinsertobject)
                    connection.commit()

                urlcomic = results[j]['urls']
                for k in range(len(urlcomic)):
                    queryurlcomic = """INSERT INTO urlcomic(idcomic,typeurl,url) VALUES(%s,%s,%s)"""
                    recordurlcomic = (id,urlcomic[k]['type'],urlcomic[k]['url'])
                    cursor.execute(queryurlcomic, recordurlcomic)
                    connection.commit()

                pricescomic = results[j]['prices']
                for k in range(len(pricescomic)):
                    querypricecomic = """INSERT INTO pricecomic(idcomic,typeprice,price) VALUES (%s,%s,%s)"""
                    recorpricecomic = (id,pricescomic[k]['type'],pricescomic[k]['price'])
                    cursor.execute(querypricecomic, recorpricecomic)
                    connection.commit()

            elif nombreendpoint == 'characters':
                querycharacter = """INSERT INTO character(idcharacterapi,namecharacter,description,modified,resourceuri,
                timeconsult,timesave,endpoint,nameapi) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
                tiempoguardadocharacter = datetime.datetime(int(time.strftime("%Y")), int(time.strftime("%m")),
                                                            int(time.strftime("%d")), int(time.strftime("%H")),
                                                            int(time.strftime("%M")),
                                                            int(time.strftime("%S")))
                recordinsertcharacter = (results[j]['id'], results[j]['name'], results[j]['description'],
                                         results[j]['modified'], results[j]['resourceURI'],
                                         str(time.mktime(tiempoconsulta.timetuple())),
                                         str(time.mktime(tiempoguardadocharacter.timetuple())), baseapi + 'characters',
                                         nombreapi)
                cursor.execute(querycharacter, recordinsertcharacter)
                connection.commit()

                urlcharacter = results[j]['urls']
                for k in range(len(urlcharacter)):
                    queryurlcharacter = """INSERT INTO urlcharacter(idcharacter,typeurl,url) VALUES(%s,%s,%s)"""
                    recordurlcharacter = (id, urlcharacter[k]['type'], urlcharacter[k]['url'])
                    cursor.execute(queryurlcharacter, recordurlcharacter)
                    connection.commit()

            elif nombreendpoint == 'creators':
                querycreator = """INSERT INTO creator(idcreatorapi,firstname,middlename,lastname,suffix,fullname,
                modified,resourceuri,timeconsult,timesave,endpoint,nameapi) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,
                %s,%s)"""
                tiempoguardadocreator = datetime.datetime(int(time.strftime("%Y")), int(time.strftime("%m")),
                                                          int(time.strftime("%d")), int(time.strftime("%H")),
                                                          int(time.strftime("%M")),
                                                          int(time.strftime("%S")))
                recordinsetcreator = (results[j]['id'], results[j]['firstName'], results[j]['middleName'],
                                      results[j]['lastName'], results[j]['suffix'], results[j]['fullName'],
                                      results[j]['modified'], results[j]['resourceURI'],
                                      str(time.mktime(tiempoconsulta.timetuple())),
                                      str(time.mktime(tiempoguardadocreator.timetuple())), baseapi + 'creators',
                                      nombreapi)
                cursor.execute(querycreator, recordinsetcreator)
                connection.commit()

                urlcreator = results[j]['urls']
                for k in range(len(urlcreator)):
                    queryurlcreator = """INSERT INTO urlcreator(idcreator,typeurl,url) VALUES(%s,%s,%s)"""
                    recordurlcreator = (id, urlcreator[k]['type'], urlcreator[k]['url'])
                    cursor.execute(queryurlcreator, recordurlcreator)
                    connection.commit()

            elif nombreendpoint == 'events':
                queryevent = """INSERT INTO event(ideventapi,title,description,modified,resourceuri,startevent,endevent,
                timeconsult,timesave,endpoint,nameapi) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
                tiempoguardadoevent = datetime.datetime(int(time.strftime("%Y")), int(time.strftime("%m")),
                                                        int(time.strftime("%d")), int(time.strftime("%H")),
                                                        int(time.strftime("%M")),
                                                        int(time.strftime("%S")))
                recordinsetevent = (results[j]['id'], results[j]['title'], results[j]['description'],
                                    results[j]['modified'], results[j]['resourceURI'], results[j]['start'],
                                    results[j]['end'], str(time.mktime(tiempoconsulta.timetuple())),
                                    str(time.mktime(tiempoguardadoevent.timetuple())), baseapi + 'events', nombreapi)
                cursor.execute(queryevent, recordinsetevent)
                connection.commit()

                urlevent = results[j]['urls']
                for k in range(len(urlevent)):
                    queryurlevent = """INSERT INTO urlevent(idevent,typeurl,url) VALUES(%s,%s,%s)"""
                    recordurlevent = (id, urlevent[k]['type'], urlevent[k]['url'])
                    cursor.execute(queryurlevent, recordurlevent)
                    connection.commit()

            elif nombreendpoint == 'series':
                queryserie = """INSERT INTO serie(idserieapi,title,description,resourceuri,typeserie,modified,startyear,
                endyear,timeconsult,timesave,endpoint,nameapi) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
                tiempoguardadoserie = datetime.datetime(int(time.strftime("%Y")), int(time.strftime("%m")),
                                                        int(time.strftime("%d")), int(time.strftime("%H")),
                                                        int(time.strftime("%M")),
                                                        int(time.strftime("%S")))
                recordinsetserie = (results[j]['id'], results[j]['title'], results[j]['description'],
                                    results[j]['resourceURI'], results[j]['type'], results[j]['modified'],
                                    results[j]['startYear'], results[j]['endYear'],
                                    str(time.mktime(tiempoconsulta.timetuple())),
                                    str(time.mktime(tiempoguardadoserie.timetuple())), baseapi + 'series', nombreapi)
                cursor.execute(queryserie, recordinsetserie)
                connection.commit()

                urlserie = results[j]['urls']
                for k in range(len(urlserie)):
                    queryurlserie = """INSERT INTO urlserie(idserie,typeurl,url) VALUES(%s,%s,%s)"""
                    recordurlserie = (id, urlserie[k]['type'], urlserie[k]['url'])
                    cursor.execute(queryurlserie, recordurlserie)
                    connection.commit()

            elif nombreendpoint == 'stories':
                querystory = """INSERT INTO story(idstoryapi,title,description,resourceuri,typestory,modified,
                timeconsult,timesave,endpoint,nameapi) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
                tiempoguardadostory = datetime.datetime(int(time.strftime("%Y")), int(time.strftime("%m")),
                                                        int(time.strftime("%d")), int(time.strftime("%H")),
                                                        int(time.strftime("%M")),
                                                        int(time.strftime("%S")))
                recordinsetstory = (results[j]['id'], results[j]['title'], results[j]['description'],
                                    results[j]['resourceURI'], results[j]['type'], results[j]['modified'],
                                    str(time.mktime(tiempoconsulta.timetuple())),
                                    str(time.mktime(tiempoguardadostory.timetuple())), baseapi + 'stories', nombreapi)
                cursor.execute(querystory, recordinsetstory)
                connection.commit()

                issuesstory = results[j]['originalIssue']
                queryissuestory = """INSERT INTO issuestory(idstory,resourceuri,nameissue) VALUES(%s,%s,%s)"""
                recordissuestory = (id, issuesstory['resourceURI'], issuesstory['name'])
                cursor.execute(queryissuestory, recordissuestory)
                connection.commit()
            else:
                print("Endpoint desconocido: Error!")


def iniciarcargadatos(accesskeyid, secretaccesskey, nombrebucket, arregloendpoint, arregloiteraciones, connectionbdrds,
                      cursorbdrds, apibase, apinombre):
    for i in range(len(arregloendpoint)):
        funcioncargabd(accesskeyid, secretaccesskey, nombrebucket, arregloendpoint[i], arregloiteraciones[i],
                       connectionbdrds, cursorbdrds, apibase, apinombre)
        print("Carga exitosa. OK!")


listsendpoints = ['characters', 'comics', 'creators', 'events', 'series', 'stories']
listnumeroiteraciones = [1, 1, 1, 1, 1, 1]
iniciarcargadatos(ACCESSKEYID, SECRETACCESSKEY, NOMBREBUCKET, listsendpoints, listnumeroiteraciones, connectionbd,
                  cursorbd, BASEAPI, NOMBREAPI)

print("PROCESO FINALIZADO :)")
